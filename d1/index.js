console.log("Hi! Alpha")

// alert("We are going to simulate an interactive web page using DOM and fetching da from a seever!")

console.log('fetch() method in JS is used to seend requests in the server and load the received responses in the webpage. The request and response is in JSON format');


fetch('https://jsonplaceholder.typicode.com/posts')
.then((res)=> res.json())
.then((data)=>showPosts(data));

//add post data

document.querySelector('#form-add-post').addEventListener('submit',(e)=>{

    e.preventDefault();

    fetch('https://jsonplaceholder.typicode.com/posts',{
        method: 'POST',
        body: JSON.stringify({
            title: document.querySelector('#txt-title').value,
            body: document.querySelector('#txt-body').value,
            userId: 1
        }),
        headers: {'Content-Type': 'application/json; charset=UTF-8'}
    })
    .then((res)=>res.json())
    .then((data)=>{

        console.log(data);
        alert('Successfully added.')

        document.querySelector('#txt-title').value = null;
        document.querySelector('#txt-body').value = null;

    })


})

//Show post 

const showPosts = (posts) => {

    let postEntries = '';

    posts.forEach((post)=>{

        postEntries += `
        
            <div id="post-${post.id}">
                <h1 id="post-title-${post.id}">${post.title}</h1>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onclick="editPost('${post.id}')">Edit</button>
                <button onclick="deletPost('${post.id}')">Delete</button>
            </div>
        `;
    })

        document.querySelector('#div-post-entries').innerHTML = postEntries;

}

fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((res)=> res.json())
.then((data)=>console.log(data));

//editPost

const editPost = (id) => {

    let title = document.querySelector(`#post-title-${id}`).innerHTML;

    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
    document.querySelector('#btn-submit-update').removeAttribute('disabled');

}

//update post

document.querySelector('#form-edit-post').addEventListener('submit',(e)=>{

    e.preventDefault();

    fetch('https://jsonplaceholder.typicode.com/posts/1',{
        method: 'PUT',
        body: JSON.stringify({
            id: document.querySelector('#txt-edit-id').value,
            title: document.querySelector('#txt-edit-title').value,
            body: document.querySelector('#txt-edit-body').value,
            userId: 1
        }),
        headers: {'Content-Type': 'application/json; charset=UTF-8'}
    })
    .then((res)=>res.json())
    .then((data)=>{

        console.log(data);
        alert('Successfully added.')

        document.querySelector('#txt-edit-id').value = null;
        document.querySelector('#txt-edit-title').value = null;
        document.querySelector('#txt-edit-body').value = null;
        document.querySelector('#btn-submit-update').setAttribute('disabled',true)

    })


})

//Activity s49
const deletPost = (id) => {
    document.querySelector(`#post-${id}`).innerHTML = '';
    document.querySelector(`#post-${id}`).remove();
}